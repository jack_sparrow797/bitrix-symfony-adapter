<?php

use App\DependencyInjection\Compiler\RegisterExceptionProcessorsPass;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class AppKernel
 */
class AppKernel extends Kernel
{
    /**
     * Папка проекта (path of the project's composer file)
     */
    protected const PROJECT_DIR = '/local/php_interface/application';

    /**
     * Папка с конфигами сайта
     */
    protected const CONFIG_DIR = '/config';

    /**
     * @var string|null
     */
    private string $documentRoot;

    /**
     * @param string $documentRoot
     * @param string $environment
     * @param bool   $debug
     */
    public function __construct(string $documentRoot, string $environment, bool $debug)
    {
        $this->documentRoot = $documentRoot;

        parent::__construct($environment, $debug);
    }

    /**
     * @return string
     */
    protected function getDocumentRoot(): string
    {
        return $this->documentRoot;
    }

    /**
     * Returns an array of bundles to register.
     *
     * @return BundleInterface[] An array of bundle instances
     */
    public function registerBundles(): iterable
    {
        $contents = require dirname(__DIR__) . '/app/bundles.php';
        foreach ($contents as $class => $envs) {
            if ($envs[$this->environment] ?? $envs['all'] ?? false) {
                yield new $class();
            }
        }
    }

    /**
     * Loads the container configuration.
     *
     * @param LoaderInterface $loader A LoaderInterface instance
     *
     * @throws Throwable
     */
    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(
            function (ContainerBuilder $container) {
                $container->setParameter('container.autowiring.strict_mode', true);
                $container->setParameter('container.dumper.inline_class_loader', true);

                $container->addObjectResource($this);
            }
        );

        $loader->load(
            $this->getConfigDir() . '/config_' . $this->getEnvironment() . '.yml'
        );
    }

    /**
     * @return string
     */
    public function getProjectDir(): string
    {
        $dir = getenv('PROJECT_DIR') ?: static::PROJECT_DIR;

        return $this->getDocumentRoot() . $dir;
    }

    /**
     * @return string
     */
    protected function getConfigDir(): string
    {
        return $this->getProjectDir() . static::CONFIG_DIR;
    }

    protected function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new RegisterExceptionProcessorsPass());
    }
}
