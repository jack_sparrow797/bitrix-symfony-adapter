<?php
// Тип среды. Поддерживаемые значения: dev, stage, prod
putenv('APP_ENV=prod');
putenv('DEBUG=true');

// Директория для логов
putenv('WWW_LOG_DIR=' . dirname(__DIR__) . '/var/log');