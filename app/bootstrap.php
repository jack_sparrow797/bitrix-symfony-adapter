<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

// Переменные окружения проекта по умолчанию
include_once __DIR__ . '/.env.php';
// Локальное переопределение переменных окружения проекта
if (file_exists(__DIR__ . '/.env.local.php')) {
    include_once __DIR__ . '/.env.local.php';
}
