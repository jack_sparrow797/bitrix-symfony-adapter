# Проект для подключения symfony к bitrix

Проект разработан на основе symfony/skeleton: 5.4
В пакете уже есть:

- symfony/console
- serializer
- validator
- translation
- paramConverter для работы с json
- exception_listener
- logger
- команда очистка кеша Битрикса

## Установка

Рекомендуется установку проводить в /local/php_interface.

```bash
$ cd local/php_interface
$ composer create-project jack797/bitrix-symfony-adapter application
```

После нужно настроить приложение. Для этого переходим в созданное приложение.

```bash
$ cd application/
```

По умолчанию, приложение создает точку входа в апи (папка api) в папке web. Созданную папку api можно перенести в корень
битрикса.
Проверить, что в bin/console DOCUMENT_ROOT определен верно

```php
$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = dirname(__DIR__, 4);
```

Подключить файл app/bootstrap.php в init.php
```php
include_once ($_SERVER["DOCUMENT_ROOT"]. '/local/php_interface/application/config/bootstrap.php');
```

Создать файл app/.env.local.php на основе app/.env.local.php.dist и настроить переменные.

```php
<?php
// Тип среды. Поддерживаемые значения: dev, stage, prod
putenv('APP_ENV=dev');
// путь до проекта, по умолчанию настроен на /local/php_interface/application
// если он отличается, то надо объявить переменную и переопределить
//putenv('PROJECT_DIR=/local/php_interface/application');
```

Для проверки работоспособности, можно запустить команду и проверить версию symfony

```bash
$ php bin/console -V 
```

Для проверки Апи, есть установленный контроллер HomeController. Команда выведет список доступных методов

```bash
$ php bin/console debug:router 
```

Чтобы проверить апи, есть установленный request web/home.http для тестирования апи. PhpStorm может с ним работать. Если
все ок, то вернется ответ, также можно проверить как работает валидация полей

## Структура проекта

- app - основные файлы для настройки подключения к битриксу symfony
  - .env.local.php.dist  - образец для создания .env.local.php - для переопределения основных настроек
  - .env - для основных настроек
  - AppKernel.php - основной файл для создания симфони приложения
  - bootstrap.php - подключение vendor, env
  - bundles.php - подключение бандлов
- bin - папка для консольных приложений (console, migrate)
  - console - symfony/console
- config - папка с конфигами приложения
- src(App) - код проекта
- translations - для перевода, сейчас настроено на locale ru  и основные переводы будут работать по умолчанию
- vendor - пакеты
- web - вспомогательные файлы, после установки битрикса, можно удалить
  - api - папка, в которой установлена точка входа для Апи, можно скопировать в корень битрикса
    - index.php - точка входа для апи
  - home.http - для проверки работы приложения, можно удалить
- composer.json - покеты композера
- composer.lock - lock композера
- symfony.lock - lock symfony 
    
