<?php

namespace App\Application;

use ReflectionClass;
use ReflectionException;

/**
 * Class Env
 *
 * @package App\Application
 */
final class Env
{
    public const PROD = 'prod';
    public const DEV = 'dev';
    public const STAGE = 'stage';
    public const TEST = 'test';

    /**
     * @return string
     */
    public static function getServerType(): string
    {
        // брать значение из куки - небезопасно
        $env = $_SERVER['APP_ENV'] ?? $_SERVER['HTTP_APP_ENV'] ?? getenv('APP_ENV') ?? self::PROD;

        return in_array($env, self::getEnvList(), true) ? $env : self::PROD;
    }
    
    /**
     * @return array
     */
    public static function getEnvList(): array
    {
        static $envList;

        if (!$envList) {
            try {
                $envList = (new ReflectionClass(self::class))->getConstants();
            } catch (ReflectionException $e) {
                $envList = ['undefined'];
            }
        }
        
        return $envList;
    }
    
    /**
     * @return bool
     */
    public static function isProd(): bool
    {
        return self::getServerType() === self::PROD;
    }
    
    /**
     * @return bool
     */
    public static function isDev(): bool
    {
        return self::getServerType() === self::DEV;
    }
    
    /**
     * @return bool
     */
    public static function isStage(): bool
    {
        return self::getServerType() === self::STAGE;
    }
    
    /**
     * @return bool
     */
    public static function isTest(): bool
    {
        return self::getServerType() === self::TEST;
    }

    /**
     * @return bool
     */
    public static function getDebugFlag(): bool
    {
        return !self::isProd();
    }
}
