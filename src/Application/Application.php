<?php

namespace App\Application;

use AppKernel;
use Bitrix\Main\Context;
use CSite;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Application
 *
 * @package App\Application
 */
final class Application
{
    protected const DEFAULT_SITE_ID = 's1';

    /** @var Application|null */
    private static ?Application $instance = null;

    /** @var Context */
    private Context $context;

    /** @var string|null */
    private ?string $siteDomainName = null;

    /** @var string|null */
    private ?string $siteDomain = null;

    /** @var AppKernel|null */
    private ?AppKernel $appKernel = null;

    /**
     * @param Context|null $context
     */
    public function __construct(Context $context = null)
    {
        /** @noinspection NullPointerExceptionInspection */
        $this->context = $context ?? \Bitrix\Main\Application::getInstance()->getContext();
    }

    /**
     * @return Application
     */
    public static function getInstance(): Application
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $name
     * @param bool $toString
     * @return false|string
     */
    public function getEnv(string $name, bool $toString = false)
    {
        $result = false;
        $name = trim($name);
        if ($name !== '') {
            $result = getenv($name, true) ?: getenv($name);
            if ($result === false && isset($_ENV[$name])) {
                $result = $_ENV[$name];
            }
        }

        return $result === false && $toString ? '' : $result;
    }

    /**
     * @return Context
     */
    public function getContext(): Context
    {
        return $this->context;
    }

    /**
     * @return string
     */
    public function getDocumentRoot(): string
    {
        return $this->getContext()->getApplication()::getDocumentRoot();
    }

    /**
     * @param string $path
     * @return string
     */
    public function getAbsolutePath(string $path): string
    {
        return $this->getDocumentRoot() . $path;
    }

    /**
     * Возвращает название домена
     * Пример: site.ru
     *
     * @return string
     */
    public function getSiteDomainName(): string
    {
        if ($this->siteDomainName === null) {
            $this->siteDomainName = $this->getContext()->getServer()->getHttpHost();
            if (!$this->siteDomainName) {
                // в cli нет HTTP_HOST, пробуем через константу
                if (defined('SITE_SERVER_NAME')) {
                    $this->siteDomainName = trim(SITE_SERVER_NAME);
                }
                // ... или через переменную окружения
                if (!$this->siteDomainName) {
                    $this->siteDomainName = trim(getenv('WEB_HOST', true) ?: getenv('WEB_HOST') ?: '');
                }
                // ... или через сайт
                if (!$this->siteDomainName) {
                    // В методе есть встроенный кеш, работает быстрее, чем через \Bitrix\Main\SiteTable::getList()
                    $site = CSite::GetList(
                        'SORT',
                        'ASC',
                        [
                            'ACTIVE' => 'Y',
                            //'DEFAULT' => 'Y',
                        ]
                    )->Fetch();
                    if ($site) {
                        $this->siteDomainName = $site['SERVER_NAME'];
                    }
                }
            }
            if (!$this->siteDomainName) {
                $this->siteDomainName = '';
            } else {
                // Через parse_url очищаем домен от возможного порта в значении.
                // Добавляем http в начале, чтобы функция отработала как мы ожидаем.
                /** @noinspection HttpUrlsUsage */
                $this->siteDomainName = parse_url('http://' . trim($this->siteDomainName), PHP_URL_HOST);
            }
        }

        return $this->siteDomainName;
    }

    /**
     * Возвращает схему с хостом без слеша на конце
     * Пример: https://site.ru
     *
     * @return string
     */
    public function getSiteDomain(): string
    {
        if ($this->siteDomain === null) {
            $context = $this->getContext();
            $result = sprintf(
                'http%s://%s',
                $context->getRequest()->isHttps() ? 's' : '',
                $this->getSiteDomainName()
            );
            $this->siteDomain = rtrim($result, '/');
        }

        return $this->siteDomain;
    }

    /**
     * @param string $url
     * @return string
     */
    public function getAbsUrl(string $url = ''): string
    {
        return sprintf('%s/%s', $this->getSiteDomain(), ltrim($url, '/'));
    }

    /**
     * @return AppKernel
     */
    public function getKernel(): AppKernel
    {
        if ($this->appKernel === null) {
            // Если второй параметр true, то будет проверяться валидность кеша, иначе - не будет.
            // Следует иметь это в виду при деплое и не забывать сбрасывать кеш (<project_dir>/var/cache).
            $this->appKernel = new AppKernel(
                $this->getDocumentRoot(),
                Env::getServerType(),
                Env::getDebugFlag()
            );
            $this->appKernel->boot();
        }

        return $this->appKernel;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->getKernel()->getContainer();
    }

    /**
     * Возвращает код сайта.
     * При установленном первом аргументе вернет сайт по умолчанию (даже для админки).
     *
     * @param bool $defaultOnEmpty
     * @return string
     */
    public function getSiteId(bool $defaultOnEmpty = true): string
    {
        $siteId = $this->getContext()->getSite();
        if (!$siteId && $defaultOnEmpty) {
            $siteId = self::DEFAULT_SITE_ID;
        }

        return $siteId;
    }
}
