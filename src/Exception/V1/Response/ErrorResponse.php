<?php

namespace App\Exception\V1\Response;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ErrorResponse
 * @package App\AppBundle\Dto\Response
 *
 * @Serializer\AccessorOrder("custom", custom = {"message", "data"})
 */
class ErrorResponse
{
    /**
     * Сообщение
     *
     * @Serializer\Type("string")
     * @Serializer\SkipWhenEmpty()
     *
     * @var string
     */
    protected $message;

    /**
     * Массив ошибок
     *
     * @var array
     */
    protected $errors = [];

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return ErrorResponse
     */
    public function setMessage(string $message): ErrorResponse
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     *
     * @return ErrorResponse
     */
    public function setErrors(array $errors): ErrorResponse
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Массив ошибок
     *
     * @Serializer\Type("array")
     *
     * @return array
     */
    public function getData(): array
    {
        return ['errors' => $this->getErrors()];
    }
}
