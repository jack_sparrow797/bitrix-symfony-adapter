<?php

namespace App\Exception\V1\Base\Validation;

use Exception;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

/**
 * Class ValidationException
 *
 * @package App\Application\Exception
 */
class ValidationException extends Exception implements ApplicationException
{
    /**
     * @var ConstraintViolationList
     */
    protected $violations;

    /**
     * @param ConstraintViolationListInterface $violations
     * @param string                           $message
     * @param int                              $code
     * @param null|Throwable                   $previous
     */
    public function __construct(
        ConstraintViolationListInterface $violations,
        string                           $message = '',
        int                              $code = 0,
        Throwable                        $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    /**
     * @return ConstraintViolationList
     */
    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
