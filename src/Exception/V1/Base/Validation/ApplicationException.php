<?php

namespace App\Exception\V1\Base\Validation;

use Throwable;

/**
 * Interface ApplicationException
 *
 * @package App\Application\Exception
 */
interface ApplicationException extends Throwable
{
}
