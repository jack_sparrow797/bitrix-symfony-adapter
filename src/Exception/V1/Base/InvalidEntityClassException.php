<?php

namespace App\Exception\V1\Base;

use UnexpectedValueException;

/**
 * Class InvalidEntityClass
 *
 * @package Adv\AdvApplication\Exception
 */
class InvalidEntityClassException extends UnexpectedValueException implements AppExceptionInterface
{

}
