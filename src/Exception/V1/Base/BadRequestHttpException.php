<?php

namespace App\Exception\V1\Base;

use Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException as BaseBadRequestHttpException;

/**
 * Class BadRequestHttpException
 *
 * @package App\AppBundle\Exception
 */
class BadRequestHttpException extends BaseBadRequestHttpException implements RequestExceptionInterface
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @param string         $message
     * @param int            $code
     * @param array          $errors
     * @param Exception|null $previous
     */
    public function __construct($message = '', $code = 0, array $errors = [], Exception $previous = null)
    {
        parent::__construct($message, $previous, $code);
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
