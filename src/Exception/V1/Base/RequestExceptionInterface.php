<?php

namespace App\Exception\V1\Base;

/**
 * Interface RequestExceptionInterface
 *
 * @package App\AppBundle\Exception
 */
interface RequestExceptionInterface extends AppExceptionInterface
{
    /**
     * @return array
     */
    public function getErrors(): array;
}
