<?php

namespace App\Exception\V1\Base;

use Throwable;

/**
 * Interface AppExceptionInterface
 *
 * @package App\AppBundle\Exception
 */
interface AppExceptionInterface extends Throwable
{
}
