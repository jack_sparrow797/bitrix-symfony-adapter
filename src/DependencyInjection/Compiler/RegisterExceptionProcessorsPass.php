<?php

namespace App\DependencyInjection\Compiler;

use App\ExceptionProcessor\V1\ExceptionProcessorInterface;
use App\Registry\ExceptionProcessorRegistryInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterExceptionProcessorsPass
 */
class RegisterExceptionProcessorsPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        $container->registerForAutoconfiguration(ExceptionProcessorInterface::class)
                  ->addTag('app.exception_processor');

        $registry = $container->getDefinition(ExceptionProcessorRegistryInterface::class);
        foreach ($container->findTaggedServiceIds('app.exception_processor') as $id => $tags) {
            $registry->addMethodCall(
                'register',
                [
                    new Reference($id),
                ]
            );
        }
    }
}
