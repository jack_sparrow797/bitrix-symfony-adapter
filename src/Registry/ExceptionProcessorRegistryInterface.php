<?php

namespace App\Registry;

use App\ExceptionProcessor\V1\ExceptionProcessorInterface;
use App\ExceptionProcessor\V1\ProcessorNotFoundException;
use Throwable;

/**
 * Class ExceptionProcessorRegistry
 * @package Adv\AppBundle\Registry
 */
interface ExceptionProcessorRegistryInterface
{
    /**
     * @param ExceptionProcessorInterface $processor
     */
    public function register(ExceptionProcessorInterface $processor): void;

    /**
     * @param Throwable $exception
     *
     * @return ExceptionProcessorInterface
     * @throws ProcessorNotFoundException
     */
    public function get(Throwable $exception): ExceptionProcessorInterface;
}
