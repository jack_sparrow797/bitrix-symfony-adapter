<?php

namespace App\Registry;


use App\ExceptionProcessor\V1\ExceptionProcessorInterface;
use App\ExceptionProcessor\V1\ProcessorNotFoundException;
use Doctrine\Common\Collections\ArrayCollection;
use Throwable;

/**
 * Class ExceptionProcessorRegistry
 * @package App\AppBundle\Registry
 */
class ExceptionProcessorRegistry implements ExceptionProcessorRegistryInterface
{
    /**
     * @var ArrayCollection|ExceptionProcessorInterface[]
     */
    protected $items;

    /**
     * ExceptionProcessorRegistry constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @param ExceptionProcessorInterface $processor
     */
    public function register(ExceptionProcessorInterface $processor): void
    {
        $this->items->add($processor);
    }

    /**
     * @param Throwable $exception
     *
     * @return ExceptionProcessorInterface
     * @throws ProcessorNotFoundException
     */
    public function get(Throwable $exception): ExceptionProcessorInterface
    {
        foreach ($this->items as $item) {
            if ($item->supports($exception)) {
                return $item;
            }
        }

        throw new ProcessorNotFoundException(
            sprintf('No processors found for "%s"', get_class($exception))
        );
    }
}
