<?php

declare(strict_types = 1);

namespace App\Service\V1;

use App\Exception\V1\Base\InvalidEntityClassException;
use App\Exception\V1\Base\RequestExceptionInterface;
use App\Exception\V1\Response\ErrorResponse;
use JMS\Serializer\ArrayTransformerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Class ErrorResponseService
 *
 * @package Adv\AdvApplication\Service
 */
class ErrorResponseService
{
    /**
     * @var ArrayTransformerInterface
     */
    protected $arrayTransformer;

    /**
     * ErrorResponseService constructor.
     *
     * @param ArrayTransformerInterface $arrayTransformer
     */
    public function __construct(ArrayTransformerInterface $arrayTransformer)
    {
        $this->arrayTransformer = $arrayTransformer;
    }

    /**
     * @param Throwable $exception
     *
     * @return bool
     */
    public function isHttpException(Throwable $exception): bool
    {
        return $exception instanceof HttpException;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function getResponseClassFromRequest(Request $request): string
    {
        return JsonResponse::class;
    }

    /**
     * @param string        $responseClass
     * @param HttpException $exception
     * @param string        $message
     *
     * @return Response
     * @throws InvalidEntityClassException
     */
    public function httpExceptionResponseFactory(string $responseClass, HttpException $exception, ?string $message = null): Response
    {
        $errors = $exception instanceof RequestExceptionInterface ? $exception->getErrors() : [];

        return $this->responseFactory($responseClass, $message, $exception->getStatusCode(), $errors);
    }

    /**
     * @param string $responseClass
     * @param string $message
     * @param int    $status
     * @param array  $errors
     *
     * @return Response
     * @throws InvalidEntityClassException
     */
    public function responseFactory(string $responseClass, ?string $message = null, ?int $status = null, array $errors = []): Response
    {
        /**
         * @todo normal factory
         */
        if ($responseClass === JsonResponse::class) {
            $dto = (new ErrorResponse())->setMessage($message)
                                        ->setErrors($errors);
            $response = new $responseClass($this->arrayTransformer->toArray($dto), $status);
        } else {
            $response = new $responseClass($message ?? '', $status);
        }

        $this->checkResponseClass($responseClass, $response);

        return $response;
    }

    /**
     * @param string $responseClass
     * @param        $response
     *
     * @throws InvalidEntityClassException
     */
    protected function checkResponseClass(string $responseClass, $response): void
    {
        if (!$response instanceof Response) {
            throw new InvalidEntityClassException(
                \sprintf(
                    'Invalid response class: %s was expected, but %s found.',
                    ResponseInterface::class,
                    $responseClass
                )
            );
        }
    }
}
