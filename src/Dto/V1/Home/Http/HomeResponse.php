<?php

namespace App\Dto\V1\Home\Http;

use JMS\Serializer\Annotation as Serializer;

class HomeResponse
{

    /**
     * @Serializer\Type("App\Dto\V1\Home\Http\HomeRequest")
     */
    private HomeRequest $request;

    /**
     * @return HomeRequest
     */
    public function getRequest(): HomeRequest
    {
        return $this->request;
    }

    /**
     * @param HomeRequest $request
     *
     * @return HomeResponse
     */
    public function setRequest(HomeRequest $request): HomeResponse
    {
        $this->request = $request;

        return $this;
    }

}