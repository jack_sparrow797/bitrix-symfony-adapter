<?php

namespace App\Dto\V1\Home\Http;

use Symfony\Component\Validator\Constraints as Assert;

class HomeRequest
{

    /**
     * @Assert\Type("string")
     */
    private string $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private string $email;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private string $phone;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     */
    private int $productId;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return HomeRequest
     */
    public function setName(string $name): HomeRequest
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return HomeRequest
     */
    public function setEmail(string $email): HomeRequest
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return HomeRequest
     */
    public function setPhone(string $phone): HomeRequest
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     *
     * @return HomeRequest
     */
    public function setProductId(int $productId): HomeRequest
    {
        $this->productId = $productId;

        return $this;
    }


}