<?php

namespace App\Command\Bitrix;

use Bitrix\Main\Composite\Page;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use CBitrixComponent;
use Exception;

class BitrixClearCacheCommand extends SymfonyCommand
{
    protected static $defaultName = 'bitrix:cache:clear';

    protected function configure(): void
    {
        $this->setDescription('Сбрасывает кеш bitrix');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws Exception
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        global $CACHE_MANAGER;
        global $stackCacheManager;

        BXClearCache(true);
        $CACHE_MANAGER->CleanAll();
        $stackCacheManager->CleanAll();
        $page = Page::getInstance();
        if ($page !== null) {
            $page->deleteAll();
        }

        $CACHE_MANAGER->CleanDir('menu');
        CBitrixComponent::clearComponentCache('bitrix:menu');

        /** @noinspection HtmlUnknownTag */
        $output->writeln('<info>Cache has been cleared</info>');

        return static::SUCCESS;
    }
}