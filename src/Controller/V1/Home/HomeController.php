<?php

namespace App\Controller\V1\Home;

use App\Controller\V1\BaseController;
use App\Dto\V1\Home\Http\HomeRequest;
use App\Dto\V1\Home\Http\HomeResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    /**
     * @param HomeRequest $homeRequest
     *
     * @return JsonResponse
     *
     * @Route("/home", name="home", methods={"GET"})
     */
    public function home(HomeRequest $homeRequest): JsonResponse
    {
        $response = (new HomeResponse())->setRequest($homeRequest);

        return $this->toJson($response);
    }
}