<?php

declare(strict_types=1);

namespace App\Controller\V1;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class BaseController
 *
 * @package App\Application\Controller
 */
class BaseController extends AbstractController
{

    private SerializerInterface $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    protected function toJson($data): JsonResponse
    {
        $json = $this->serializer->toArray($data);

        return new JsonResponse($json, 200);
    }
}
