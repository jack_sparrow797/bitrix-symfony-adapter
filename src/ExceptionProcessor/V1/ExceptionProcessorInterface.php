<?php

namespace App\ExceptionProcessor\V1;


use App\Exception\V1\Base\BadRequestHttpException;
use App\Exception\V1\Base\RequestExceptionInterface;
use Throwable;

/**
 * Interface ExceptionProcessorInterface
 * @package App\AppBundle\ExceptionProcessor
 */
interface ExceptionProcessorInterface
{
    /**
     * @param Throwable $exception
     *
     * @return BadRequestHttpException
     */
    public function process(Throwable $exception): RequestExceptionInterface;

    /**
     * @param Throwable $exception
     *
     * @return bool
     */
    public function supports(Throwable $exception): bool;
}