<?php

namespace App\ExceptionProcessor\V1;

use LogicException;

/**
 * Class ProcessorNotFoundException
 * @package Adv\AppBundle\Exception
 */
class ProcessorNotFoundException extends LogicException implements AppExceptionInterface
{

}
