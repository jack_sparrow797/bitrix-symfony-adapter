<?php

namespace App\ExceptionProcessor\V1;

use App\Exception\V1\Base\Validation\ValidationException;
use Throwable;

class ValidationExceptionProcessor extends AbstractBadRequestExceptionProcessor
{
    /**
     * @inheritDoc
     */
    protected function getMessage(Throwable $exception): string
    {
        return 'Ошибка валидации параметров запроса';
    }

    /**
     * @inheritDoc
     */
    protected function getErrors(Throwable $exception): array
    {
        /** @var ValidationException $exception */
        return [$exception->getViolations()];
    }

    protected function getSupportedExceptions(): array
    {
        return [ValidationException::class];
    }
}