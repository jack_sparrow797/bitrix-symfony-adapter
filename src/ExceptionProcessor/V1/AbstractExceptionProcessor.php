<?php

namespace App\ExceptionProcessor\V1;

use App\Exception\V1\Base\BadRequestHttpException;
use App\Exception\V1\Base\RequestExceptionInterface;
use Throwable;

/**
 * Class AbstractExceptionProcessor
 * @package App\AppBundle\ExceptionProcessor
 */
abstract class AbstractExceptionProcessor implements ExceptionProcessorInterface
{
    /**
     * @param Throwable $exception
     *
     * @return BadRequestHttpException
     */
    public function process(Throwable $exception): RequestExceptionInterface
    {
        return $this->createException(
            $this->getExceptionClass(),
            $this->getMessage($exception),
            $this->getErrorCode(),
            $this->getErrors($exception)
        );
    }

    /**
     * @param Throwable $exception
     *
     * @return bool
     */
    public function supports(Throwable $exception): bool
    {
        $result = false;
        foreach ($this->getSupportedExceptions() as $class) {
            $result |= $exception instanceof $class;
        }

        return $result;
    }

    /**
     * @param string $exceptionClass
     * @param string $message
     * @param int    $errorCode
     * @param array  $errors
     *
     * @return RequestExceptionInterface
     */
    protected function createException(
        string $exceptionClass,
        string $message,
        int    $errorCode,
        array  $errors
    ): RequestExceptionInterface {
        return new $exceptionClass($message, $errorCode, $errors);
    }

    /**
     * @return string
     */
    abstract protected function getExceptionClass(): string;

    /**
     * Сообщение (строка либо код translation'а)
     *
     * @param Throwable $exception
     *
     * @return string
     */
    abstract protected function getMessage(Throwable $exception
    ): string;/** @noinspection PhpUnusedParameterInspection */

    /**
     * Массив ошибок (напр. ошибок валидации)
     *
     * @param Throwable $exception
     *
     * @return array
     */
    protected function getErrors(Throwable $exception): array
    {
        return [];
    }

    /**
     * Массив классов обрабатываемых исключений
     *
     * @return string[]
     */
    abstract protected function getSupportedExceptions(): array;

    /**
     * Параметры для сообщения об ошибке
     *
     * @return string[]
     */
    protected function getMessageParameters(): array
    {
        return [];
    }

    /**
     * Код ошибки
     *
     * @return int
     */
    protected function getErrorCode(): int
    {
        return 0;
    }
}