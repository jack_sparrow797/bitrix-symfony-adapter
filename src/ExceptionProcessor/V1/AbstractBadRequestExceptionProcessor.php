<?php

namespace App\ExceptionProcessor\V1;


use App\Exception\V1\Base\BadRequestHttpException;

/**
 * Class AbstractBadRequestExceptionProcessor
 * @package App\AppBundle\ExceptionProcessor
 */
abstract class AbstractBadRequestExceptionProcessor extends AbstractExceptionProcessor
{
    /**
     * @return string
     */
    final protected function getExceptionClass(): string
    {
        return BadRequestHttpException::class;
    }
}