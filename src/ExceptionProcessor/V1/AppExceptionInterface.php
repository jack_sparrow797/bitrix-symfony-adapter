<?php

namespace App\ExceptionProcessor\V1;

use Throwable;

/**
 * Interface AppExceptionInterface
 *
 * @package App\AppBundle\Exception
 */
interface AppExceptionInterface extends Throwable
{
}
