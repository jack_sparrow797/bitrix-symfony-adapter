<?php

declare(strict_types=1);

namespace App\Listener;


use App\Exception\V1\Base\InvalidEntityClassException;
use App\ExceptionProcessor\V1\ProcessorNotFoundException;
use App\Registry\ExceptionProcessorRegistryInterface;
use App\Service\V1\ErrorResponseService;
use Psr\Log\LoggerAwareTrait;
use InvalidArgumentException;
use Psr\Log\LoggerAwareInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Class ExceptionListener
 *
 * @package Adv\AdvApplication
 */
class ExceptionListener implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    /**
     * @var ErrorResponseService
     */
    private $errorResponseService;

    /**
     * @var ExceptionProcessorRegistryInterface
     */
    private $processorRegistry;

    /**
     * @var bool
     */
    private $showExceptions;

    /**
     * ExceptionListener constructor.
     *
     * @param ErrorResponseService                $errorResponseService
     * @param ExceptionProcessorRegistryInterface $processorRegistry
     * @param bool                                $showExceptions
     */
    public function __construct(
        ErrorResponseService $errorResponseService,
        ExceptionProcessorRegistryInterface $processorRegistry,
        bool $showExceptions
    )
    {
        $this->errorResponseService = $errorResponseService;
        $this->processorRegistry = $processorRegistry;
        $this->showExceptions = $showExceptions;
    }

    /**
     * Show a bitrix 404 page on the Production and log non-HTTP exceptions
     *
     * @param ExceptionEvent $responseEvent
     *
     * @throws InvalidArgumentException
     * @throws InvalidEntityClassException
     *
     * @die
     */
    public function onUnhandledException(ExceptionEvent $responseEvent): void
    {
        $exception = $responseEvent->getThrowable();

        try {
            $processor = $this->processorRegistry->get($exception);
            $exception = $processor->process($exception);
        } catch (ProcessorNotFoundException $e) {
        }

        if ($this->errorResponseService->isHttpException($exception)) {
            /** @noinspection PhpParamsInspection */
            $responseEvent->setResponse(
                $this->errorResponseService->httpExceptionResponseFactory(
                    $this->errorResponseService->getResponseClassFromRequest($responseEvent->getRequest()),
                    $exception,
                    $exception->getMessage()
                )
            );

            $this->logger->critical(
                sprintf('Unhandled exception: %s: %s', \get_class($exception), $exception->getMessage()),
                ['trace' => $exception->getTrace()]
            );

            return;
        }

        $this->logger->critical(
            sprintf('Unhandled exception: %s: %s', \get_class($exception), $exception->getMessage()),
            ['trace' => $exception->getTrace()]
        );

        if (!$this->showExceptions) {
            $responseEvent->setResponse(
                $this->errorResponseService->responseFactory(
                    $this->errorResponseService->getResponseClassFromRequest($responseEvent->getRequest()),
                    'Неизвестная ошибка. Пожалуйста, попробуйте позднее или свяжитесь с нами.',
                    500
                )
            );
        }
    }

//    /**
//     * @param int $page
//     */
//    private function requireErrorPage(int $page = null): void
//    {
//        $pageFile = \sprintf(
//            '%s/%d.php',
//            AppKernel::getDocumentRoot(),
//            $page
//        );
//
//        $pageFile = file_exists($pageFile)
//            ? $pageFile
//            : \sprintf(
//                '%s/500.html',
//                AppKernel::getDocumentRoot()
//            );
//
//        /** @noinspection PhpIncludeInspection */
//        require_once $pageFile;
//    }
}
