<?php

namespace App\ParamConverter\Home;

use App\Dto\V1\Home\Http\HomeRequest;
use App\ParamConverter\AbstractJsonParamConverter;

class HomeParamConverter extends AbstractJsonParamConverter
{

    protected function getDtoClass(): string
    {
        return HomeRequest::class;
    }
}