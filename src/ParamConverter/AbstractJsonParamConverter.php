<?php
declare(strict_types=1);

namespace App\ParamConverter;

use App\Exception\V1\Base\BadRequestHttpException;
use App\Exception\V1\Base\Validation\ValidationException;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\SerializerInterface;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractJsonParamConverter
 *
 * Базовый ParamConverter для десериализации тела запроса в требуемый Dto.
 * Выполняет валидацию сразу после десериализации.
 * При ошибке десериализации выбрасывает BadRequestHttpException
 *
 * @package App\Application\ParamConverter
 */
abstract class AbstractJsonParamConverter extends AbstractParamConverter
{
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;

    /**
     * @var ValidatorInterface
     */
    protected ValidatorInterface $validator;

    /**
     * @param ValidatorInterface  $validator
     * @param SerializerInterface $serializer
     */
    public function __construct(ValidatorInterface $validator, SerializerInterface $serializer)
    {
        parent::__construct($validator);

        $this->serializer = $serializer;
    }

    /**
     * @param Request        $request
     * @param ParamConverter $configuration
     *
     * @throws ValidationException
     * @throws LogicException
     * @throws BadRequestHttpException
     *
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $params = $this->convert(
            $this->getJsonData($request)
        );
        $params = $this->beforeValidate($params);
        $this->validate($params);
        $params = $this->afterValidate($params);

        $request->attributes->set($configuration->getName(), $params);

        return true;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getJsonData(Request $request): string
    {
        return $request->getContent();
    }

    /**
     * @param string $data
     *
     * @throws LogicException
     * @throws BadRequestHttpException
     * @return object
     */
    protected function convert(string $data): object
    {
        try {
            return $this->serializer->deserialize(
                $data,
                $this->getDtoClass(),
                'json',
                $this->getContext()
            );
        } catch (RuntimeException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    /**
     * @param object $params
     *
     * @throws ValidationException
     */
    protected function validate(object $params): void
    {
        $violations = $this->validator->validate(
            $params,
            $this->getValidatorConstraints(),
            $this->getValidatorGroups(),
        );

        if ($violations->count()) {
            throw new ValidationException($violations);
        }
    }

    /**
     * @return null|DeserializationContext
     */
    protected function getContext(): ?DeserializationContext
    {
        return null;
    }

    abstract protected function getDtoClass(): string;

    /**
     * @return Constraint[]|null
     */
    protected function getValidatorConstraints(): ?array
    {
        return null;
    }

    /**
     * @return string[]|GroupSequence[]|null
     */
    protected function getValidatorGroups(): ?array
    {
        return null;
    }

    protected function afterValidate(object $params): object
    {
        return $params;
    }

    protected function beforeValidate(object $params): object
    {
        return $params;
    }
}
