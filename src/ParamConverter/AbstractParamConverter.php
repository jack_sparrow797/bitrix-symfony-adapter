<?php
declare(strict_types=1);

namespace App\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractParamConverter
 *
 * @package App\Application\ParamConverter
 */
abstract class AbstractParamConverter implements ParamConverterInterface
{
    /**
     * @var ValidatorInterface
     */
    protected ValidatorInterface $validator;

    /**
     * @param ValidatorInterface  $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator  = $validator;
    }

    /**
     * @inheritdoc
     */
    abstract public function apply(Request $request, ParamConverter $configuration): bool;

    /**
     * @param ParamConverter $configuration
     *
     * @return bool
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === $this->getDtoClass();
    }

    /**
     * @return string
     */
    abstract protected function getDtoClass(): string;
}
