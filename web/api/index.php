<?php
/** @noinspection DuplicatedCode */
/** @noinspection PhpMultipleClassDeclarationsInspection */

declare(strict_types=1);

use App\Application\Application;
use Bitrix\Main\Context;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

defined('NO_KEEP_STATISTIC') || define('NO_KEEP_STATISTIC', true);
defined('STOP_STATISTICS') || define('STOP_STATISTICS', true);
defined('NOT_CHECK_PERMISSIONS') || define('NOT_CHECK_PERMISSIONS', true);
defined('NO_AGENT_CHECK') || define('NO_AGENT_CHECK', true);
defined('NO_AGENT_STATISTIC') || define('NO_AGENT_STATISTIC', 'Y');
defined('DisableEventsCheck') || define('DisableEventsCheck', true);
defined('BX_NO_ACCELERATOR_RESET') || define('BX_NO_ACCELERATOR_RESET', true);
defined('B24CONNECTOR_SKIP') || define('B24CONNECTOR_SKIP', true);
defined('PULL_AJAX_INIT') || define('PULL_AJAX_INIT', true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include.php';

$symfonyKernel = Application::getInstance()->getKernel();
$request = Request::createFromGlobals();

try {
    $response = $symfonyKernel->handle($request);

    /**
     * Отправка заголовков и cookie "Битрикса"
     * @todo Это временное решение. Найти способ как совместить отправку ответа Symfony и Битрикса
     *      с сохранением их функциональности.
     */
    Context::getCurrent()->getResponse()->writeHeaders();

    /**
     * Отправка заголовков и cookie "Symfony".
     * После этого метода заголовки больше отправляться не будут.
     */
    $response->send();

    $symfonyKernel->terminate($request, $response); // Здесь генерируется событие "kernel.terminate"

    // В FinalActions генерируется событие OnBeforeEndBufferContent, выполняется $response-send()
    // c отправкой заголовков и cookie, затем вызывается terminate().
    // В terminate генерируется событие OnAfterEpilog, выполняются фоновые задания и закрывается соединение с БД.
    // В конце вызывается exit().
    // Жертвуем событием OnBeforeEndBufferContent, чтобы не выполнять вхолостую $response-send()
    /*
     CMain::FinalActions();
    /*/
    \Bitrix\Main\Application::getInstance()->terminate();
    //*/
} catch (Throwable $throwable) {
    /** @var LoggerInterface $logger */
    if (!defined('ERROR_HANDLER_REGISTERED') && ($logger = $symfonyKernel->getContainer()->get('logger'))) {
        $logger->error(
            sprintf(
                "[%s] %s (%s)\n%s\n",
                get_class($throwable),
                $throwable->getMessage(),
                $throwable->getCode(),
                $throwable->getTraceAsString()
            )
        );
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    throw $throwable;
}
